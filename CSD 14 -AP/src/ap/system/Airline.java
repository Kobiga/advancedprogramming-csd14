package ap.system;

import java.util.ArrayList;



public class Airline {
	private String id;
	private String name;
	
	private ArrayList <Flight>flight;
    private ArrayList<Aircraft>aircraft;
	public Airline(String id, String name, ArrayList<Flight> flight) {
		super();
		this.id = id;
		this.name = name;
		this.flight = flight;
	}
  
	
	public ArrayList<Aircraft> getAircraft() {
		return aircraft;
	}


	public void setAircraft(ArrayList<Aircraft> aircraft) {
		this.aircraft = aircraft;
	}


	public Airline(String id, String name, ArrayList<Flight> flight, ArrayList<Aircraft> aircraft) {
		super();
		this.id = id;
		this.name = name;
		this.flight = flight;
		this.aircraft = aircraft;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Flight> getFlight() {
		return flight;
	}

	public void setFlight(ArrayList<Flight> flight) {
		this.flight = flight;
	}

	@Override
	public String toString() {
		return "Airline [id=" + id + ", name=" + name + ", flight=" + flight + ", aircraft=" + aircraft + "]";
	}
	
}
