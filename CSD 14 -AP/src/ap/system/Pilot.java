package ap.system;

import java.util.ArrayList;

public class Pilot {
	private  String id;
	private String name;
	ArrayList<Flight>flight;
	ArrayList<Co_Pilot>pilot1;
	public Pilot(String id, String name, ArrayList<Flight> flight) {
		super();
		this.id = id;
		this.name = name;
		this.flight = flight;
	}
	
	public Pilot(String id, String name, ArrayList<Flight> flight, ArrayList<Co_Pilot> pilot1) {
		super();
		this.id = id;
		this.name = name;
		this.flight = flight;
		this.pilot1 = pilot1;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Flight> getFlight() {
		return flight;
	}
	public void setFlight(ArrayList<Flight> flight) {
		this.flight = flight;
	}
	
	
	public ArrayList<Co_Pilot> getPilot1() {
		return pilot1;
	}

	public void setPilot1(ArrayList<Co_Pilot> pilot1) {
		this.pilot1 = pilot1;
	}

	
	@Override
	public String toString() {
		return "Pilot [id=" + id + ", name=" + name + ", flight=" + flight + ", pilot1=" + pilot1 + "]";
	}
	
	

}
