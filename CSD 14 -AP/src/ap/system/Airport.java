package ap.system;

import java.util.ArrayList;

public class Airport {
	private String id;
	private String name;
	private ArrayList <Flight>flight;
	public Airport(String id, String name, ArrayList<Flight> flight) {
		super();
		this.id = id;
		this.name = name;
		this.flight = flight;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Flight> getFlight() {
		return flight;
	}
	public void setFlight(ArrayList<Flight> flight) {
		this.flight = flight;
	}
	@Override
	public String toString() {
		return "Airport [id=" + id + ", name=" + name + ", flight=" + flight + "]";
	}
	
	

}
