package ap.system;

import java.util.ArrayList;

public class AircraftType {
	private String name_of_type;
	private ArrayList<Aircraft>aircraft;

	public AircraftType(String name_of_type, ArrayList<Aircraft> aircraft) {
		super();
		this.name_of_type = name_of_type;
		this.aircraft = aircraft;
	}

	public String getName_of_type() {
		return name_of_type;
	}

	public void setName_of_type(String name_of_type) {
		this.name_of_type = name_of_type;
	}

	public ArrayList<Aircraft> getAircraft() {
		return aircraft;
	}

	public void setAircraft(ArrayList<Aircraft> aircraft) {
		this.aircraft = aircraft;
	}

	@Override
	public String toString() {
		return "AircraftType [name_of_type=" + name_of_type + ", aircraft=" + aircraft + "]";
	}
	

}
