package ap.system;

import java.util.ArrayList;

public class Flight {
	private  String id;
	private  String arrival_time;
	private String departure_time;
	private ArrayList <Airport>airport;
	
	public Flight(String id) {
		super();
		this.id = id;
	}
	public Flight(String id, String arrival_time, String departure_time) {
		super();
		this.id = id;
		this.arrival_time = arrival_time;
		this.departure_time = departure_time;
	}
	
	public Flight(String id, String arrival_time, String departure_time, ArrayList<Airport> airport) {
		super();
		this.id = id;
		this.arrival_time = arrival_time;
		this.departure_time = departure_time;
		this.airport = airport;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getArrival_time() {
		return arrival_time;
	}
	public void setArrival_time(String arrival_time) {
		this.arrival_time = arrival_time;
	}
	public String getDeparture_time() {
		return departure_time;
	}
	public void setDeparture_time(String departure_time) {
		this.departure_time = departure_time;
	}
	public ArrayList<Airport> getAirport() {
		return airport;
	}
	public void setAirport(ArrayList<Airport> airport) {
		this.airport = airport;
	}
	@Override
	public String toString() {
		return "Flight [id=" + id + ", arrival_time=" + arrival_time + ", departure_time=" + departure_time + "]";
	}

	

}
