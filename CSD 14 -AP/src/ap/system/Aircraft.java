package ap.system;

public class Aircraft {
	private String id;
	private String repair;
	
	private String landed;

	public Aircraft(String id, String repair, String landed) {
		super();
		this.id = id;
		this.repair = repair;
		this.landed = landed;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRepair() {
		return repair;
	}

	public void setRepair(String repair) {
		this.repair = repair;
	}

	public String getLanded() {
		return landed;
	}

	public void setLanded(String landed) {
		this.landed = landed;
	}

	@Override
	public String toString() {
		return "Aircraft [id=" + id + ", repair=" + repair + ", landed=" + landed + "]";
	}
	
}
