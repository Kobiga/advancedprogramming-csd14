package ap.isa;

public class ISAdemo {
	public static void main(String[] args) {
		
		Animals animal = new Animals();
		System.out.println("-------------------");
		Mammal mammal = new Mammal();
		System.out.println("-------------------");
		Dog dog = new Dog();
		System.out.println("-------------------");
		Repitile rep = new Repitile();
		
		boolean status = Mammal.class.isInstance(dog);
		System.out.println(status);
		
		 status =mammal.getClass().isInstance(rep);
		 System.out.println(status);
		 
		 status =dog.getClass().isInstance(rep);
		 System.out.println(status);
		
		 
		 System.out.println(animal.getClass().isInstance(rep));
		 System.out.println(animal.getClass().isInstance(dog));
		 System.out.println(animal.getClass().isInstance(mammal));
		// System.out.println(animal.getClass().isInstance(animal));
	}

}
