package EnumEx;
import EnumEx.Seasons;

public class SeasonDemo {
	public static void main(String[] args) {
		Seasons s=  Seasons.SUMMER;
		
		switch(s) {
		case WINTER:
			System.out.println("Very cool");
			break;
		case SPRING:
			System.out.println("Sunny and pleasant.");
			break;
		case SUMMER:
			System.out.println("Hot");
			break;
			default:
				System.out.println("Pick the one season");
				
		}
		//System.out.println(s);
	}

}
