package com.ex;
public class Person {
	String name;//instance variable
	//static int age;//class varialble or static variable
	int age;
	
	public Person(String name) {
		this.name= name;
		System.out.println(name+" created");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public  void setAge(int age) {
		this.age = age;
	}

	public void sayHello(Person person) {
		System.out.println(getName()+"said hello "+person.name);
		
	}
	public void sayBye(Person person) {
		System.out.println(getName()+" say bye "+person.getName());
		
	}

}
