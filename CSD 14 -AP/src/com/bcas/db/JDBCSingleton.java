package com.bcas.db;

	import java.sql.Connection;
	import java.sql.DriverManager;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.SQLException;

	import com.mysql.cj.jdbc.DatabaseMetaData;

	public class JDBCSingleton {

		private static String url ="jdbc:mysql://localhost:3306/";
		private static String username ="root";
		private static String password ="KOBI8324";
		private static String database="csd14";
		
		private static JDBCSingleton jdbc = null;
		
		private JDBCSingleton() {
			
		}
		
		//providing public point of access
		public static JDBCSingleton getInstance() {
			if(jdbc == null) {
				jdbc = new JDBCSingleton();
			}
			return jdbc;
		}
		
		public static Connection getConnection() throws ClassNotFoundException, SQLException {
			
			Connection connection = null;
		//	Class.forName("com.mysql.jdbc.Drive");
			connection = DriverManager.getConnection(DBProperties.URL.value +  DBProperties.DATABASE.value, DBProperties.USERNAME.value, DBProperties.PASSWORD.value);
			return connection;
		}
		
		// to insert the table record into the database
		public void insert(String name,String address) throws ClassNotFoundException, SQLException {
			
			Connection connection = null;
			PreparedStatement statement = null;
			
			
			connection = getConnection();
			statement = connection.prepareStatement("INSERT INTO campus(name,address)VALUES(?,?)");
			statement.setString(1, name);
			statement.setString(2, address);
			int rowCount = statement.executeUpdate();
			
			if(statement != null) {
				statement.close();
			}
			if(connection != null) {
				connection.close();
			}
			
		}	
			// view data form table
			public void view (String name) throws ClassNotFoundException, SQLException {
				
				Connection connection = null;
				PreparedStatement statement = null;
				ResultSet resultSet = null;
				
				connection = getConnection();
				statement = connection.prepareStatement("SELECT * FROM campus WHERE name = ?" );
				statement.setString(1, name);
				resultSet = statement.executeQuery();
				
				while(resultSet.next()) {
					System.out.println("Campus Name = " + resultSet.getString(1) + "\n" + "Campus Address = " + resultSet.getString(2));
				}
				
				if(statement != null) {
					statement.close();
				}
				if(connection != null) {
					connection.close();
				}
			
			 
		}
			
			public void delete(String name) throws ClassNotFoundException, SQLException{
				Connection connection=null;
				connection=getConnection();
				int rowCount= connection.prepareStatement("DELETE FROM campus WHERE name ='"+name+"'").executeUpdate();
				if(connection !=null) {
					connection.close();
				}
				System.out.println("Row count :" +rowCount);
	}
			public void update(String name, String address) throws ClassNotFoundException, SQLException {
				Connection connection = null;
				PreparedStatement statement = null;
				
				connection = getConnection();
				int count = connection.prepareStatement("UPDATE campus SET address = '"+address+"' WHERE name = '"+name+"'").executeUpdate();
				
				
				if(connection != null) {
					connection.close();
				}
	}
	}
