package com.fcatory.Kottu;

public class Demo {
	public static void main(String[] args) {
		Kottu plainKottu= new PlainKottu();
		System.out.println(plainKottu.make());
		
		Kottu vegKottu= new VegKottu(new PlainKottu());
		System.out.println("Vegetable Kottu--->"+vegKottu.make());
			
		
		Kottu eggKottu= new EggKottu(new VegKottu(new PlainKottu()));
		System.out.println("Egg Kottu--->"+eggKottu.make());
		
		Kottu MeatKottu=new MeatKottu(new EggKottu(new VegKottu(new PlainKottu())),Meat.CHICKEN);
		System.out.println("MeatKottu---->"+MeatKottu.make());
			
			
	}

}
