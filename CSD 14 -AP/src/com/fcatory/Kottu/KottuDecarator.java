package com.fcatory.Kottu;

public class KottuDecarator implements Kottu{
	
	protected Kottu customizedKottu;
	
	public KottuDecarator(Kottu kottu) {
		customizedKottu=kottu;
	}

	@Override
	public String make() {
		
		return customizedKottu.make();
	}
	

}
