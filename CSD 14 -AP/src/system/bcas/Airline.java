package system.bcas;
public class Airline {
	private String id;
	private String name;
	
	
	public Airline(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Airline [name=" + name + ", id=" + id + "]";
	}
	
	

}
