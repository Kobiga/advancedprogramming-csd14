package bcas.fcatory.creationalPattern;

public class Admin extends Workers {

	public Admin() {
		super(WorkType.ADMIN);
		arrangeProgram();
	}

	@Override
	protected void arrangeProgram() {
		System.out.println("Prepare the lecture time schedule");
		
	}

}
