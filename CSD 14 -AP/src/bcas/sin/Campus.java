package bcas.sin;

public class Campus {
	private String name;
	private static  Campus campus=new Campus();
	
	private Campus() {
		
	}
	public static Campus createInstance() {
		return campus;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Campus [name=" + name + "]";
	}
	

}
