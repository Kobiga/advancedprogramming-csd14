package bcas.carfactory.creationalPattern;

public class CarFactory {
	public static Car buildcar(CarType model) {
		
		Car car = null;
		switch (model) {
		case SMALL:
			car= new SmallCar();
			break;
		case LUXURY:
			car= new LuxuryCar();
			break;
		case SEDAN:
			car = new SedanCar();
			break;
			
		}
		return car; 
		
	}

}
