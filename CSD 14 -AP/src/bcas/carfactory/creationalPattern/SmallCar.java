package bcas.carfactory.creationalPattern;

public class SmallCar extends Car{

	public SmallCar() {
		super(CarType.SMALL);
		constract();
	}

	@Override
	protected void constract() {
		System.out.println("Buildgin small car");
		
	}

}
