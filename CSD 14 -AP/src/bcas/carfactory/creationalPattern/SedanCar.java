package bcas.carfactory.creationalPattern;

public class SedanCar  extends Car{

	public SedanCar() {
		super(CarType.SEDAN);
		constract();
	}

	@Override
	protected void constract() {
		System.out.println("Buildgin Sedan car");
		
	}

}
