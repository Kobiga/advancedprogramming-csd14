package bcas.carfactory.creationalPattern;

public class TestCarFactory {
	public static void main(String[] args) {
		
		CarFactory.buildcar(CarType.LUXURY);
	}

}
