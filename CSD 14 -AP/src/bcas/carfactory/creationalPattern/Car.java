package bcas.carfactory.creationalPattern;

public  abstract class Car {
	protected CarType model;
	
	protected abstract void constract();
	public Car(CarType model) {
		this.model=model;
		arrangeParts();
		
	}
	
	private void arrangeParts() {
		System.out.println("Arrange parts for "+model);
	}
	public CarType getModel() {
		return model;
	}
	public void setModel(CarType model) {
		this.model = model;
	}
	

}
