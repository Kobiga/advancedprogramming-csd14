package bcas.carfactory.creationalPattern;

public class LuxuryCar extends Car {

	public LuxuryCar() {
		super(CarType.LUXURY);
		constract();
	}

	@Override
	protected void constract() {
		System.out.println("Building Luxury Car");
	}
	

}
