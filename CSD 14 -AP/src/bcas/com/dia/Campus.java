package bcas.com.dia;

import java.util.ArrayList;

public class Campus {
	private String name;
	private String address;
	private final int no_of_student=200;
	private ArrayList <Student>student;
	
	public Campus(String name, String address, ArrayList<Student> student) {
		super();
		this.name = name;
		this.address = address;
		this.student = student;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getNo_of_student() {
		return no_of_student;
	}
	
	public ArrayList<Student> getStudent() {
		return student;
	}
	public void setStudent(ArrayList<Student> student) {
		this.student = student;
	}
	@Override
	public String toString() {
		return "Campus [name=" + name + ", address=" + address + ", no_of_student=" + no_of_student + ", student="
				+ student + "]";
	}
	
	
	}
	


