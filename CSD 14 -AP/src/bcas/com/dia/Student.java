package bcas.com.dia;

import java.util.ArrayList;

public class Student {
	
		private String name;
		private String address;
		private int age;
		private String  batch;
		
		//ArrayList<Student>student;
		public Student(String name, String address, int age, String batch) {
			super();
			this.name = name;
			this.address = address;
			this.age = age;
			this.batch = batch;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
		public String getBatch() {
			return batch;
		}
		public void setBatch(String batch) {
			this.batch = batch;
		}
		@Override
		public String toString() {
			return "Student [name=" + name + ", address=" + address + ", age=" + age + ", batch=" + batch + "]";
		}
		
		

}
