package bcas.oop.poly.ex;


public class Student {
	
	private String name;
	private String address;
	private int age;
	private char grade;
	
	public Student() {
		
	}
	public Student(String name) {
		this.name=name;
		
	}
	public Student(String name, String address) {
		this.name=name;
		this.address=address;
		
	}
	public Student(String name, String address,int age) {
		this.name=name;
		this.address=address;
		this.age=age;
		
	}
	public Student(String name, String address,int age, char grade) {
		this.name=name;
		this.address=address;
		this.age=age;
		this.grade=grade;
		
}
	

	public void printDetail() {
		System.out.println("Name:"+ this.name  );
		System.out.println("address:"+this.address);
		System.out.println("age:"+this.age);
		System.out.println("grade:"+this.grade);
		System.out.println("................................................");
	}
	



}
