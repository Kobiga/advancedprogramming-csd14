package bcas.oop.poly;

public class CalculatorDemo {
	public static void main(String[] args) {
		Calculator cal= new Calculator();
		System.out.println(cal.add(10, 20));
		System.out.println(cal.add(10, 20,50));
		System.out.println(cal.add(1.3,40));
		System.out.println(cal.add(1,4.0));
		
	}

}
