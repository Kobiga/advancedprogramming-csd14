package bcas.oop.enc;

public class StudentInformation {
	private String name;
	private int age;
	private final String campusName="BCAS";
	
	public String getCampusName() {
		return campusName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	

}
