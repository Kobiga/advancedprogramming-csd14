package bcas.oop.inter.ex;

public class Triangel implements Shape {
	
	double base=10;
	double side1 =7;
	double side2=3;
	double height=5;
	String color ="Green";
	boolean isfilled=true;

	@Override
	public double getArea() {
		//return 1/2*base*height;
		return  (base *height)/2;
	}

	@Override
	public double getPermeter() {
		
		return side1+side2+base;
	}

	@Override
	public boolean isFilled() {
		
		return isfilled;
	}

	@Override
	public String getColor() {
		
		return color;
	}

}
