package bcas.oop.inter.ex;

public class Rectangel implements Shape{
	double width=10;
	double height=5;
	String color ="Red";
	boolean isfilled=true;

	@Override
	public double getArea() {
		
		return width*height;
	}

	@Override
	public double getPermeter() {
		
		return 2*(width*height);
	}

	@Override
	public boolean isFilled() {
		
		return isfilled;
	}

	@Override
	public String getColor() {
		
		return color;
	}

}
