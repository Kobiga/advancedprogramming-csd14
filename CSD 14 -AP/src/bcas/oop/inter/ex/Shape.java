package bcas.oop.inter.ex;

public interface Shape {
	
	public double getArea();
	
	public double getPermeter();
	
	public boolean isFilled();
	
	public String getColor();


}
