package bcas.oop.inter.ex;

public class Square implements Shape {
	
	double width=10;
	String color="Blue";
	boolean isfilled=true;
	

	public double getArea() {
		
		return width*width;
	}

	@Override
	public double getPermeter() {
		
		return 4*width;
	}

	@Override
	public boolean isFilled() {
		
		return isfilled;
	}

	@Override
	public String getColor() {
		
		return color;
	}

	
	
}
