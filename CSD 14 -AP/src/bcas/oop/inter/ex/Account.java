package bcas.oop.inter.ex;

public class Account {

	private double balance;
	private String accNumber;
	
	public Account (String accNumber) {
		this.accNumber=accNumber;
		
	}
	public void deposite (double amount) throws MaxDepositeException,MinDepositeException, SufficientWithdrawException{
		if(amount <100) {
			throw new MinDepositeException();
		}
		else if(amount>100000) {
			throw new MinDepositeException();
		}
		else {
			balance +=amount ;
		}
	}
	public void withdraw (double Wamount) throws   InSufficientWithdrawException{
		if(Wamount <balance) {
			balance -=Wamount ;
		}
		else {
			double needs= Wamount-balance;
			throw new  InSufficientWithdrawException(needs); // InsufficientWithdrawException = new InsufficientWithdrawException
		}
		
		
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public String getAccNumber() {
		return accNumber;
	}
	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}
	
	
}
