package bcas.behavioural;

public class AverageScoreDisplay implements Observer {
	private float runRate;
	private int predictScore;
	
	public void update(int runs, int wickets, float overs) {
		runRate=runs/overs;
		predictScore=(int)(runRate*50);
		display();
		
	}
	public void display() {
		System.out.println("Average Score Display: \n"+ "Run Rate:"+runRate+"\npredict Score: "+ predictScore);
	}

}
