package bcas.behavioural;

public class Demo {
	public static void main(String[] args) {
		CurrentScoreDisplay currentScoreDisplay=new CurrentScoreDisplay();
		AverageScoreDisplay averageScoreDisplay= new AverageScoreDisplay();
		
		CricketData cricketData= new CricketData();
		cricketData.registerObserver(currentScoreDisplay);
		cricketData.registerObserver(averageScoreDisplay);
		
		//cricketData.dataChanged();
		//cricketData.unregisterObserver(averageScoreDiaply);
		
		cricketData.unregisterObserver(averageScoreDisplay);
		cricketData.dataChanged();
		
		ESPNDisplay display= new ESPNDisplay();
		cricketData.registerObserver(display);
	}

}
