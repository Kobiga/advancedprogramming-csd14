package bcas.behavioural;

import java.util.ArrayList;

public class CricketData implements Subject {
	 int runs, wickets;
	 float overs;
//	 CurrentScoreDisplay currentScoreDisplay;
//	 AverageScoreDisplay averageScoreDisplay ;
	 protected ArrayList<Observer> observerList;
	 
	 public CricketData() {
		 observerList= new ArrayList<>();
		
}
	@Override
	public void registerObserver(Observer observer) {
		observerList.add(observer);
		
	}
	@Override
	public void unregisterObserver(Observer observer) {
		observerList.remove(observerList.indexOf(observer));
	}
	@Override
	public void notifyObservers() {
		for(Observer ob:observerList) {
			ob.update(runs, wickets, overs);
		}
		
		
	}
	public int getLatesRuns() {
		return 90;
		
	}
	public int getLatesWickets() {
		return 5;
		
	}
	public float getLatestOvers() {
		return (float) 11.2;
		
	}
	
	public void dataChanged() {
   runs= getLatesRuns();
	wickets=getLatesWickets();
	overs= getLatestOvers();
	
	notifyObservers();
		
		
	}
	
	}
